FROM openjdk:8-jdk
MAINTAINER Jayishnu Tripathi (jayishnu.17cse@bml.edu.in)
RUN apt-get update
RUN apt-get install -y maven
COPY pom.xml /usr/local/service/pom.xml
COPY src /usr/local/service/src
WORKDIR /usr/local/service
RUN mvn package